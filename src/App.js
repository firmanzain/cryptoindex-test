// import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import React, { useState } from 'react';

function App() {
  const [quote, setQuote] = useState("");
  const [favorite, setFavorite] = useState([]);
  const [quotetext, setQuotetext] = useState("");
  const [quotelist, setQuotelist] = useState([]);

  function getQuotes() {
    axios.get(`https://api.kanye.rest/`)
    .then(res => {
      setQuote(res.data.quote)
    })
  }

  function addFavorite() {
    let tempFavorite = favorite
    if (!tempFavorite.includes(quote)) {
      tempFavorite.push(quote)
      setFavorite([])
      setTimeout(function(){
        setFavorite(tempFavorite)
      }, 200);
    }
  }

  function handleChange(event) {
    setQuotetext(event.target.value);
  }

  function submitForm(event) {
    let tempQuotes = quotelist
    if (!tempQuotes.includes(quotetext)) {
      tempQuotes.push(quotetext)
      setQuotetext("");
      setQuotelist([])
      setTimeout(function(){
        setQuotelist(tempQuotes)
      }, 200);
    }
    event.preventDefault();
  }

  return (
    <div className="App">
      <p>{ quote }</p>
      
      <button type="button" onClick={() => getQuotes()}>
        Get Quote
      </button>
      <button type="button" onClick={() => addFavorite()}>
        Add Favorite
      </button>

      <ul>
        { 
          favorite.map((value, index) => 
            <ol key={index}> { value } </ol>
          ) 
        }
      </ul>
      
      <hr />
      <h1>My Quotes</h1>
      <form onSubmit={submitForm}>
        <input type="text" value={quotetext} onChange={handleChange} required />
        <button type="submit">Submit</button>
      </form>

      <ul>
        { 
          quotelist.map((value, index) => 
            <ol key={index}> { value } </ol>
          ) 
        }
      </ul>

    </div>
  );
}

export default App;
